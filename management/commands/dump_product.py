from django.core.management.base import BaseCommand, CommandError # custom command library
import os 
import sys
import xlrd
from decimal import Decimal
#from os.path import join, abspath, dirname
import shutil
from django.core.files import File
from product.models import *
from product.modules.configurable.models import ConfigurableProduct, ProductVariation
from django.contrib.sites.models import Site
#
##
## # Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank
##
##
class Command(BaseCommand):
    def handle(self, *args, **options):
        if args:
            #print "ok" , args[0]
            # check file exist
            if os.path.isfile(args[0]):
                if len(args) > 1:
                    if os.path.isdir(args[1]):
                        imagePath = args[1]
                        project_path = os.path.abspath(os.path.dirname(__name__))
                        project_path = os.path.join(project_path,'static')
                        project_path = os.path.join(project_path,'images')
                        # Read a xls file
                        workbook = xlrd.open_workbook(args[0] , on_demand = True)
                        #Get Sheet names
                        ########   print workbook.sheet_names()
                        # Open sheet categories
                        worksheet = workbook.sheet_by_name(u'Produits')

                        # Site name
                        site_name_ = Site.objects.get(pk=3)
                        # Grab the cell contents of each row of a worksheet: 
                        num_rows = worksheet.nrows - 1
                        num_cells = worksheet.ncols - 1
                        curr_row = -1
                        print "Inserting data....."
                        while curr_row < num_rows:
                            curr_row += 1
                            if curr_row:
                                row = worksheet.row(curr_row)
                                # check last parent
                                curr_cell = -1
                                #while curr_cell < num_cells:
                                #   curr_cell += 1
                                name = (row[3].value).encode('utf-8')
                                product_name = row[4].value
                                product_desc = (row[5].value).encode('utf-8')
                                try:
                                    obj = None
                                    try:
                                        obj = Category.objects.get(name=name)
                                    except:
                                        print name + " category not found!"
                                    #CategoryAttribute.objects.create(category=obj,languagecode='en',description=(row[4].value).encode('utf-8'))
                                    #CategoryAttribute.objects.create(category=obj,languagecode='fr',description=(row[5].value).encode('utf-8'))
                                    
                                    #obj.translations.get_or_create(languagecode='fr',description=(row[5].value).encode('utf-8'),name='Frencial')
                                    if obj:
                                        english_desc = product_desc
                                        if english_desc[0] != '[':
                                            english_desc = '<p>' + english_desc
                                            if english_desc.find(']') < len(english_desc) - 10:
                                                english_desc = english_desc[:english_desc.find(']')+1] + '<p>' + english_desc[english_desc.find(']')+1 : ]
                                            english_desc = english_desc.replace('[','</p><ul><li>')
                                            english_desc = english_desc.replace(',','</li><li>')
                                            english_desc = english_desc.replace(']','</li></ul>')
                                            if english_desc.find('['):
                                                english_desc = english_desc + '</p>'
                                        else:
                                            english_desc = english_desc.replace('[','<ul><li>')
                                            english_desc = english_desc.replace(',','</li><li>')
                                            english_desc = english_desc.replace(']','</li></ul>')
                                        
                                        product = None
                                        try:
                                            product , is_new = Product.objects.get_or_create(site=site_name_,name=product_name,description=english_desc)
                                            product.category.add(obj)
                                        except:
                                            print product_name + " Problem to create product"
                                        if product:
                                            try:
                                                product.translations.get_or_create(languagecode='en',description=english_desc,name=product_name)
                                            except:
                                                pass
                                            # price list
                                            price_1 = row[6].value
                                            price_2 = row[7].value
                                            price_3 = row[8].value
                                            price_5 = row[9].value
                                            price_10 = row[10].value
                                            price_20 = row[11].value
                                            price_25 = row[12].value
                                            price_30 = row[13].value
                                            price_50 = row[14].value
                                            ####### Price Add
                                            if price_1:
                                                product.price_set.get_or_create(price=price_1,quantity=1)
                                            if price_2:
                                                product.price_set.get_or_create(price=price_2,quantity=2)
                                            if price_3:
                                                product.price_set.get_or_create(price=price_3,quantity=3)
                                            if price_5:
                                                product.price_set.get_or_create(price=price_5,quantity=5)
                                            if price_10:
                                                product.price_set.get_or_create(price=price_10,quantity=10)
                                            if price_20:
                                                product.price_set.get_or_create(price=price_20,quantity=20)
                                            if price_25:
                                                product.price_set.get_or_create(price=price_25,quantity=25)
                                            if price_30:
                                                product.price_set.get_or_create(price=price_30,quantity=30)
                                            if price_50:
                                                product.price_set.get_or_create(price=price_50,quantity=50)
                                            #####@@@@@@@@@@@#
                                            #### Images Upload
                                            #####@@@@@@@@@@@#
                                            sort_ = 1
                                            if row[0].value:
                                                image1 = os.path.join(imagePath, row[0].value)
                                                 
                                                if os.path.isfile(image1):
                                                    shutil.copy2(image1, project_path)
                                                    dest = os.path.join('images' , row[0].value)
                                                    sort_ = 1 + len(ProductImage.objects.filter(product=product))
                                                    ProductImage.objects.create(product=product,picture= dest, sort=sort_)
                                                    sort_ = sort_ + 1
                                                    
                                                else:
                                                    extension = '.jpeg'
                                                    file_name = os.path.splitext(row[0].value)[0]
                                                    if os.path.splitext(row[0].value)[1] == '.jpg':
                                                        pass
                                                    else:
                                                        extension = '.jpg'
                                                    image1 = os.path.join(imagePath, file_name +  extension)
                                                    if os.path.isfile(image1):
                                                        shutil.copy2(image1, project_path)
                                                        dest = os.path.join('images' , file_name + extension)
                                                        sort_ = 1 + len(ProductImage.objects.filter(product=product))
                                                        ProductImage.objects.create(product=product,picture= dest, sort=sort_)
                                                        sort_ = sort_ + 1
                                                    else:
                                                        file_name = os.path.splitext(row[0].value)[0]
                                                        image1 = os.path.join(imagePath, file_name + '.png')
                                                        if os.path.isfile(image1):
                                                            shutil.copy2(image1, project_path)
                                                            dest = os.path.join('images' , file_name +  '.png')
                                                            CategoryImage.objects.create(category=obj,picture= dest, sort=sort_)
                                                            sort_ = sort_ + 1
                                                        else:
                                                            print image1 + " Image not found!"
                                            if row[1].value:
                                                image2 = os.path.join(imagePath, row[1].value)
                                                if os.path.isfile(image2):
                                                    shutil.copy2(image2, project_path)
                                                    dest = os.path.join('images' , row[1].value)
                                                    sort_ = 1 + len(ProductImage.objects.filter(product=product))
                                                    ProductImage.objects.create(product=product,picture=dest , sort=sort_)
                                                    sort_ = sort_ + 1
                                                else:
                                                    extension = '.jpeg'
                                                    file_name = os.path.splitext(row[1].value)[0]
                                                    if os.path.splitext(row[1].value)[1] == '.jpg':
                                                        pass
                                                    else:
                                                        extension = '.jpg'
                                                    image2 = os.path.join(imagePath, file_name + extension)
                                                    if os.path.isfile(image2):
                                                        shutil.copy2(image2, project_path)
                                                        dest = os.path.join('images' , file_name + extension)
                                                        sort_ = 1 + len(ProductImage.objects.filter(product=product))
                                                        ProductImage.objects.create(product=product,picture= dest, sort=sort_)
                                                        sort_ = sort_ + 1
                                                    else:
                                                        file_name = os.path.splitext(row[1].value)[0]
                                                        image2 = os.path.join(imagePath, file_name +  '.png')
                                                        if os.path.isfile(image2):
                                                            shutil.copy2(image2, project_path)
                                                            dest = os.path.join('images' , file_name +  '.png')
                                                            CategoryImage.objects.create(category=obj,picture= dest, sort=sort_)
                                                            sort_ = sort_ + 1
                                                        else:
                                                            print image2 + " Image not found!"
                                            if row[2].value:
                                                image3 = os.path.join(imagePath, row[2].value)
                                                if os.path.isfile(image3):
                                                    shutil.copy2(image3, project_path)
                                                    dest = os.path.join('images' , row[2].value)
                                                    sort_ = 1 + len(ProductImage.objects.filter(product=product))
                                                    ProductImage.objects.create(product=product,picture=dest , sort=sort_)
                                                    sort_ = sort_ + 1
                                                else:
                                                    extension = '.jpeg'
                                                    file_name = os.path.splitext(row[2].value)[0]
                                                    if os.path.splitext(row[2].value)[1] == '.jpg':
                                                        pass
                                                    else:
                                                        extension = '.jpg'
                                                    image3 = os.path.join(imagePath, file_name + extension)
                                                    if os.path.isfile(image3):
                                                        shutil.copy2(image2, project_path)
                                                        dest = os.path.join('images' , file_name + extension)
                                                        sort_ = 1 + len(ProductImage.objects.filter(product=product))
                                                        ProductImage.objects.create(product=product,picture= dest, sort=sort_)
                                                        sort_ = sort_ + 1
                                                    else:
                                                        file_name = os.path.splitext(row[2].value)[0]
                                                        image3 = os.path.join(imagePath, file_name +  '.png')
                                                        if os.path.isfile(image3):
                                                            shutil.copy2(image3, project_path)
                                                            dest = os.path.join('images' , file_name +  '.png')
                                                            sort_ = 1 + len(ProductImage.objects.filter(product=product))
                                                            ProductImage.objects.create(product=product,picture= dest, sort=sort_)
                                                            sort_ = sort_ + 1
                                                        else:
                                                            print image3 + " Image not found!"
                                    print "------------------"
                                except:
                                    print str(curr_row) + " row have Problem in excel."
                                    print "------------------"
                                    #print (row[0].value).encode('utf-8')

                                
                                #print "Next Row..."
                        
                        #print parent_track_dict
                            
                        print "Finish."
                    else:
                        print "No such image directory exist."
                else:
                    print "Please give a Images directory path"
            else:
                print "No such file exist."

        else:
            print "Please give a xls file path"
        

def _create_category(name, parent=None):
    """
    Create a category. Calls itself when hierarchy is needed.
    """
    if '/' in name:
        # parent/child categories can be created with /
        parent = None
        for hier_category in name.split('/'):
            parent = _create_category(hier_category, parent)
        return
    try:
        cat = Category.objects.get(name=name)
    except Category.DoesNotExist:
        cat = Category(name=name)
        if parent is not None:
            cat.parent = parent
        cat.site = Site.objects.get(id=1)
        cat.save()
    else:
        if parent is not None:
            cat = Category(name=name)
            cat.parent = parent
            cat.site = Site.objects.get(id=1)
            cat.save()
    return cat

def _find_category(name):
    parent = None
    for sub_name in name.split('/'):
        if parent is not None:
            category = Category.objects.get(name=sub_name, parent=parent)
        else:
            category = Category.objects.get(name=sub_name)
        parent = category
    return category

def _create_product_attribute():
    try:
        AttributeOption.objects.get(name='extra')
    except AttributeOption.DoesNotExist:
        option = AttributeOption(description='Something')
        option.name = 'extra'
        option.validation = 'product.utils.validation_simple'
        option.sort_order = 99
        option.error_message = 'extra missing'
        option.save()
    else:
        pass

def _add_product_attribute(product, attr_name, value):
    pattr = ProductAttribute(product=product)
    pattr.languagecode = 'en'
    pattr.option = AttributeOption.objects.get(name=attr_name)
    pattr.value = value
    pattr.save()

def _create_product(row):
    p = Product(name=row[3])
    p.site = Site.objects.get(id=1)
    p.sku = '%s-%s' % (int(row[0]), row[1])
    p.short_description = row[10]
    p.description = row[11]
    p.save()
    # find the category to add
    for category_name in row[4].split(','):
        category = _find_category(category_name)
        p.category.add(category)
    if row[12]:
        p.items_in_stock = Decimal(str(row[12]))
    try:
        float(row[15])
    except:
        pass
    else:
        p.weight = Decimal(str(row[15]))
    p.weight_units = row[16]
    if row[17]:
        p.length = Decimal(str(row[17]))
    p.length_units = row[18]
    if row[19]:
        p.width = Decimal(str(row[19]))
    p.width_units = row[20]
    if row[21]:
        p.height = Decimal(str(row[21]))
    p.height_units = row[22]
    p.taxClass = TaxClass.objects.get(title='Default')
    p.taxable = True
    p.save()
    # add a product attribute
    if row[2]:
        _add_product_attribute(p, 'extra', row[2])
    # add price
    price = Price(product=p)
    price.price = Decimal(str(row[8]))
    price.save()
    # add image
    if row[23]:
        image = ProductImage(product=p)
        image.picture = row[23].replace('\\', '/')
        image.save()

def _create_product_variation(row):
    """
    Manually create all objects for a new product option.
    This includes (if not existing):
        Option
        OptionGroup
        Product
        ConfigurableProduct
        ProductVariation

    You could also use the create_subs flag from ConfigurableProduct which
    creates the variations automatically for you, but you're then loosing the
    ability to manually set the SKU etc.
    """
    # get option group or create if not existing
    try:
        group = OptionGroup.objects.get(name=row[4])
    except OptionGroup.DoesNotExist:
        group = OptionGroup(site=Site.objects.get(id=1))
        group.name = row[4]
        group.description = ''
        group.save()
    # create the new option
    option = Option(option_group=group)
    option.name = row[3]
    option.value = row[3]
    option.sort_order = int(row[1])
    option.save()
    # now first create a Product...
    product = _create_product(row)
    # variants dont need a category, otherwise they show up as normal products
    product.category.clear()
    # then get or create the ConfigurableProduct and add the option group
    # somehow the root product to which all variations belong must be
    # identified, in this case the variation id is 00
    main_product = Product.objects.get(sku='%s-%s' % (int(row[0]), '00'))
    try:
        cp = ConfigurableProduct.objects.get(product=main_product)
    except ConfigurableProduct.DoesNotExist:
        cp = ConfigurableProduct(product=main_product)
        cp.save()
        cp.option_group.add(group)
        cp.save()
    # finally create the ProductVariation and add the option
    variation = ProductVariation(product=product)
    variation.parent = cp
    variation.save()
    variation.options.add(option)
    variation.save()

