from django.core.management.base import BaseCommand, CommandError # custom command library
import os 
import sys
import xlrd
from decimal import Decimal
#from os.path import join, abspath, dirname

from product.models import *
from product.modules.configurable.models import ConfigurableProduct, ProductVariation
from django.contrib.sites.models import Site
#
##
## # Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank
##
##
class Command(BaseCommand):
    def handle(self, *args, **options):
        if args:
            #print "ok" , args[0]
            # check file exist
            if os.path.isfile(args[0]):
   
                # Read a xls file
                workbook = xlrd.open_workbook(args[0] , on_demand = True)
                #Get Sheet names
                ########   print workbook.sheet_names()
                # Open sheet categories
                worksheet = workbook.sheet_by_name(u'Produits FR')

                # Site name
                #site_name_ = Site.objects.get(pk=1)
                # Grab the cell contents of each row of a worksheet: 
                num_rows = worksheet.nrows - 1
                num_cells = worksheet.ncols - 1
                curr_row = -1
                print "Inserting data....."
                while curr_row < num_rows:
                    curr_row += 1
                    if curr_row:
                        row = worksheet.row(curr_row)
                        # check last parent
                        curr_cell = -1
                        product_name = (row[0].value)
                        name = (row[2].value).encode('utf-8')
                        try:
                            obj = Category.objects.get(name=name)
                            try:
                                frencial_desc = (row[1].value).encode('utf-8')
                                if frencial_desc[0] != '[':
                                    frencial_desc = '<p>' + frencial_desc
                                    if frencial_desc.find(']') < len(frencial_desc) - 10:
                                            frencial_desc = frencial_desc[:frencial_desc.find(']')+1] + '<p>' + frencial_desc[frencial_desc.find(']')+1 : ]
                                    frencial_desc = frencial_desc.replace('[','</p><ul><li>')
                                    frencial_desc = frencial_desc.replace(',','</li><li>')
                                    frencial_desc = frencial_desc.replace(']','</li></ul>')
                                    if frencial_desc.find('['):
                                        frencial_desc = frencial_desc + '</p>'
                                else:
                                    frencial_desc = frencial_desc.replace('[','<ul><li>')
                                    frencial_desc = frencial_desc.replace(',','</li><li>')
                                    frencial_desc = frencial_desc.replace(']','</li></ul>')
                                product = Product.objects.get(name=product_name , category=obj)
                                product.translations.get_or_create(languagecode='fr',description=frencial_desc,name=product_name)
                            except:
                                print product_name + " Product not found!"
                        except:
                            print name + " category not found!"
                    
                print "Finish."

            else:
                print "No such file exist."

        else:
            print "Please give a xls file path"
        



