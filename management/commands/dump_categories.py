from django.core.management.base import BaseCommand, CommandError # custom command library
import os 
import xlrd # read xls file library
from product.models import Category # import category table
from django.contrib.sites.models import Site

#
##
## # Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank
##
##
class Command(BaseCommand):
    def handle(self, *args, **options):
        if args:
            #print "ok" , args[0]
            # check file exist
            if os.path.isfile(args[0]):
                # Read a xls file
                workbook = xlrd.open_workbook(args[0] , on_demand = True)
                #Get Sheet names
                ########   print workbook.sheet_names()
                # Open sheet categories
                worksheet = workbook.sheet_by_name(u'Cat\xe9gories')
                # Site name
                site_name_ = Site.objects.get(pk=3)
                # take user input  
                num_cols = int(raw_input("How many columns of data this excel's have? --: "))
                parent_track_dict = {}
                # Make dict for parent track dynamic
                ind_ = 0
                while ind_ < num_cols:
                    parent_track_dict[ind_] = None
                    ind_ = ind_ + 1
                # Grab the cell contents of each row of a worksheet: 
                num_rows = worksheet.nrows - 1
                num_cells = num_cols
                curr_row = -1
                print "Inserting data....."
                while curr_row < num_rows:
                    curr_row += 1
                    row = worksheet.row(curr_row)
                    # check last parent
                    ind_ = 0
                    while ind_ < num_cols:
                        if row[ind_].value:
                            if not ind_:
                                parent_track_dict[ind_] , created = Category.objects.get_or_create(site=site_name_,name=unicode(row[ind_].value))
                            else:
                                k = ind_ - 1
                                #print "parent " ,parent_track_dict[k]
                                #print "Name" ,type(row[ind_].value)
                                #print site_name_
                                #print type(parent_track_dict[k])
                                obj , created = Category.objects.get_or_create(site=site_name_,name=unicode(row[ind_].value),parent=parent_track_dict[k])
                                parent_track_dict[ind_] = obj
                                #print ind_    

                        
                        ind_ = ind_ + 1
                    #print "Next Row..."
                
                #print parent_track_dict
                    
                print "Finish."
                
            else:
                print "No such file exist."

        else:
            print "Please give a xls file path"
        
