from django.core.management.base import BaseCommand, CommandError # custom command library
import os 
import sys
import xlrd
from decimal import Decimal
#from os.path import join, abspath, dirname
import shutil
from django.core.files import File
from product.models import *
from product.modules.configurable.models import ConfigurableProduct, ProductVariation

##
## # Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank
##
##
class Command(BaseCommand):
    def handle(self, *args, **options):
        if args:
            #print "ok" , args[0]
            # check file exist
            if os.path.isfile(args[0]):
                if len(args) > 1:
                    if os.path.isdir(args[1]):
                        imagePath = args[1]
                        project_path = os.path.abspath(os.path.dirname(__name__))
                        project_path = os.path.join(project_path,'static')
                        project_path = os.path.join(project_path,'images')
                        # Read a xls file
                        workbook = xlrd.open_workbook(args[0] , on_demand = True)
                        #Get Sheet names
                        ########   print workbook.sheet_names()
                        # Open sheet categories
                        worksheet = workbook.sheet_by_name(u'Categories Data')

                        # Grab the cell contents of each row of a worksheet: 
                        num_rows = worksheet.nrows - 1
                        num_cells = worksheet.ncols - 1
                        curr_row = -1
                        print "Inserting data....."
                        while curr_row < num_rows:
                            curr_row += 1
                            if curr_row:
                                row = worksheet.row(curr_row)
                                # check last parent
                                curr_cell = -1
                                #while curr_cell < num_cells:
                                #   curr_cell += 1
                                name = row[0].value
                                try:
                                
                                    obj = None
                                    try:
                                        obj = Category.objects.get(name=name)
                                    except:
                                        print name + ' Category not found!'
                                    
                                    if obj:
                                        
                                        english_desc = (row[4].value).encode('utf-8')
                                        if english_desc[0] != '[':
                                            english_desc = '<p>' + english_desc
                                            if english_desc.find(']') < len(english_desc) - 10:
                                                english_desc = english_desc[:english_desc.find(']')+1] + '<p>' + english_desc[english_desc.find(']')+1 : ]
                                            english_desc = english_desc.replace('[','</p><ul><li>')
                                            english_desc = english_desc.replace(',','</li><li>')
                                            english_desc = english_desc.replace(']','</li></ul>')
                                            if english_desc.find('['):
                                                english_desc = english_desc + '</p>'
                                        else:
                                            english_desc = english_desc.replace('[','<ul><li>')
                                            english_desc = english_desc.replace(',','</li><li>')
                                            english_desc = english_desc.replace(']','</li></ul>')

                                        
                                        frencial_desc = (row[5].value).encode('utf-8')
                                        if frencial_desc[0] != '[':
                                            frencial_desc = '<p>' + frencial_desc
                                            if frencial_desc.find(']') < len(frencial_desc) - 10:
                                                frencial_desc = frencial_desc[:frencial_desc.find(']')+1] + '<p>' + frencial_desc[frencial_desc.find(']')+1 : ]
                                            frencial_desc = frencial_desc.replace('[','</p><ul><li>')
                                            frencial_desc = frencial_desc.replace(',','</li><li>')
                                            frencial_desc = frencial_desc.replace(']','</li></ul>')
                                            if frencial_desc.find('['):
                                                frencial_desc = frencial_desc + '</p>'
                                        else:
                                            frencial_desc = frencial_desc.replace('[','<ul><li>')
                                            frencial_desc = frencial_desc.replace(',','</li><li>')
                                            frencial_desc = frencial_desc.replace(']','</li></ul>')

                                        
                                        try:
                                            obj.translations.get_or_create(languagecode='en',description=english_desc,name=name)
                                        except:
                                            pass
                                        try:
                                            obj.translations.get_or_create(languagecode='fr',description=frencial_desc,name=name)
                                        except:
                                            pass
                                        
                                        sort_ = 1
                                        if row[1].value:
                                            image1 = os.path.join(imagePath, (row[1].value).encode('utf-8'))
                                            if os.path.isfile(image1):
                                                shutil.copy2(image1, project_path)
                                                dest = os.path.join('images' , (row[1].value).encode('utf-8'))
                                                
                                                sort_ = 1 + len(CategoryImage.objects.filter(category=obj))
                                                CategoryImage.objects.get_or_create(category=obj,picture= dest, sort=sort_)
                                                sort_ = sort_ + 1
                                                
                                            else:
                                                extension = '.jpeg'
                                                file_name = os.path.splitext((row[1].value).encode('utf-8'))[0]
                                                if os.path.splitext((row[1].value).encode('utf-8'))[1] == '.jpg':
                                                    pass
                                                else:
                                                    extension = '.jpg'
                                                image1 = os.path.join(imagePath, file_name +   extension)
                                                if os.path.isfile(image1):
                                                    shutil.copy2(image1, project_path)
                                                    dest = os.path.join('images' , file_name + '.' + extension)
                                                    
                                                    sort_ = 1 + len(CategoryImage.objects.filter(category=obj))
                                                    CategoryImage.objects.create(category=obj,picture= dest, sort=sort_)
                                                    sort_ = sort_ + 1
                                                else:
                                                    file_name = os.path.splitext((row[1].value).encode('utf-8'))[0]
                                                    image1 = os.path.join(imagePath, file_name +  '.png')
                                                    if os.path.isfile(image1):
                                                        shutil.copy2(image1, project_path)
                                                        dest = os.path.join('images' , file_name + '.png')
                                                        
                                                        sort_ = 1 + len(CategoryImage.objects.filter(category=obj))
                                                        CategoryImage.objects.get_or_create(category=obj,picture= dest, sort=sort_)
                                                        sort_ = sort_ + 1
                                                    else:
                                                        print image1 + " Image not found!"

                                        if row[2].value:
                                            image2 = os.path.join(imagePath, (row[2].value).encode('utf-8'))
                                            if os.path.isfile(image2):
                                                shutil.copy2(image2, project_path)
                                                dest = os.path.join('images' , (row[2].value).encode('utf-8'))
                                                sort_ = 1 + len(CategoryImage.objects.filter(category=obj))

                                                CategoryImage.objects.get_or_create(category=obj,picture=dest , sort=sort_)
                                                sort_ = sort_ + 1
                                            else:
                                                extension = '.jpeg'
                                                file_name = os.path.splitext((row[2].value).encode('utf-8'))[0]
                                                if os.path.splitext((row[2].value).encode('utf-8'))[1] == '.jpg':
                                                    pass
                                                else:
                                                    extension = '.jpg'
                                                image2 = os.path.join(imagePath, file_name +  extension)
                                                if os.path.isfile(image2):
                                                    shutil.copy2(image2, project_path)
                                                    dest = os.path.join('images' , file_name +  extension)
                                                    sort_ = 1 + len(CategoryImage.objects.filter(category=obj))
                                                    CategoryImage.objects.get_or_create(category=obj,picture= dest, sort=sort_)
                                                    sort_ = sort_ + 1
                                                else:
                                                    file_name = os.path.splitext((row[2].value).encode('utf-8'))[0]
                                                    image2 = os.path.join(imagePath, file_name +  '.png')
                                                    if os.path.isfile(image2):
                                                        shutil.copy2(image2, project_path)
                                                        dest = os.path.join('images' , file_name +  '.png')
                                                        sort_ = 1 + len(CategoryImage.objects.filter(category=obj))
                                                        CategoryImage.objects.get_or_create(category=obj,picture= dest, sort=sort_)
                                                        sort_ = sort_ + 1
                                                    else:
                                                        print image2 + " Image not found!"
                                        if row[3].value:
                                            image3 = os.path.join(imagePath, (row[3].value).encode('utf-8'))
                                            if os.path.isfile(image3):
                                                shutil.copy2(image3, project_path)
                                                dest = os.path.join('images' , (row[3].value).encode('utf-8'))
                                                sort_ = 1 + len(CategoryImage.objects.filter(category=obj))
                                                CategoryImage.objects.get_or_create(category=obj,picture=dest , sort=sort_)
                                                sort_ = sort_ + 1
                                            else:
                                                extension = '.jpeg'
                                                file_name = os.path.splitext((row[3].value).encode('utf-8'))[0]
                                                if os.path.splitext((row[3].value).encode('utf-8'))[1] == '.jpg':
                                                    pass
                                                else:
                                                    extension = '.jpg'
                                                image3 = os.path.join(imagePath, file_name + extension)
                                                if os.path.isfile(image3):
                                                    shutil.copy2(image3, project_path)
                                                    dest = os.path.join('images' , file_name +  extension)
                                                    sort_ = 1 + len(CategoryImage.objects.filter(category=obj))
                                                    CategoryImage.objects.get_or_create(category=obj,picture= dest, sort=sort_)
                                                    sort_ = sort_ + 1
                                                else:
                                                    file_name = os.path.splitext((row[3].value).encode('utf-8'))[0]
                                                    image3 = os.path.join(imagePath, file_name + '.png')
                                                    if os.path.isfile(image3):
                                                        shutil.copy2(image3, project_path)
                                                        dest = os.path.join('images' , file_name +  '.png')
                                                        sort_ = 1 + len(CategoryImage.objects.filter(category=obj))
                                                        
                                                        CategoryImage.objects.get_or_create(category=obj,picture= dest, sort=sort_)

                                                        sort_ = sort_ + 1
                                                    else:
                                                        print image3 + " Image not found!"
                                    print "-------------------------"       
                                except:
                                    print str(curr_row) + " row have Problem in excel."
                                    print "-------------------------"
                                #print (row[0].value).encode('utf-8')

                                
                                #print "Next Row..."
                        
                        #print parent_track_dict
                            
                        print "Finish."
                    else:
                        print "No such image directory exist."
                else:
                    print "Please give a Images directory path"
            else:
                print "No such file exist."

        else:
            print "Please give a xls file path"
        
